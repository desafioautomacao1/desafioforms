package br.com.desafio.busca;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BuscaNttPage {

	private WebDriver driver;
	private static final String URL_GOOGLE = "https://www.google.com.br/";
	ArrayList<String> handles;

	public BuscaNttPage() {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver32.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(URL_GOOGLE);
	}

	public void preencheCampoBuscaNtt() {
		driver.findElement(By.id("APjFqb")).sendKeys("NTT DATA Business Solutions");
		driver.findElement(By.id("APjFqb")).submit();
	}

	public void selecionaPrimeiroOpcaoNtt() {
		driver.findElement(By.xpath("//div[@class='HiHjCd']//a[contains(text(),'Contact us')]")).click();

	}

	public void acceptCoockie() {
		driver.findElement(By.xpath("//button[contains(text(),'All Cookies')]")).click();
	}

	public void acessarOpcaoCarreira() {
		driver.findElement(By.xpath("//li[6]//a[1]//span[contains(text(),'Careers')]")).click();
	}

	public void fazerParteDaEquipe() throws InterruptedException {
		driver.findElement(By.xpath("//a[contains(text(),'Join our team')]")).click();
		Thread.sleep(3000);
	}
	
	public void pesquisarVaga() {

		handles = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(handles.get(1));

		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("pesquisar-vaga-localidade-etc")).click();
		driver.findElement(By.id("pesquisar-vaga-localidade-etc")).sendKeys("Arquiteto");
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.quit();
	}
}
