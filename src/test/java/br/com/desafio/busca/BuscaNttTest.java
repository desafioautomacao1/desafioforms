package br.com.desafio.busca;

import org.junit.Before;
import org.junit.Test;

public class BuscaNttTest {

	private BuscaNttPage buscaNtt;

	@Before
	public void berofeForSetup() {
		this.buscaNtt = new BuscaNttPage();
	}

	@Test
	public void testeBuscaPelaNtt() throws Exception {
		buscaNtt.preencheCampoBuscaNtt();
		buscaNtt.selecionaPrimeiroOpcaoNtt();

		// modal acept coockie
		buscaNtt.acceptCoockie();
		buscaNtt.acessarOpcaoCarreira();
		buscaNtt.fazerParteDaEquipe();
		buscaNtt.pesquisarVaga();
	}

}
